#!/bin/bash -e

sed -i 's/-a nymea -p nymea-box/-a "Raspberry Pi" -p "Raspberry Pi"/' ${ROOTFS_DIR}/lib/systemd/system/nymea-networkmanager.service
